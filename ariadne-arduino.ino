#include<math.h>
#include<Wire.h>
#include<EEPROM.h>
#include<SoftwareSerial.h>

#define mpu95200address  0x68
#define magaddress  0x0C
#define M1 9 
#define M2 3 
#define M3 11 
#define M4 10 
#define Buzzer 5

SoftwareSerial bluetooth(8, 7); // RX | TX


int a, x, y, z, i, p, delayA, delayB, offsetX, offsetY, offsetZ, maxX, minX, maxY, minY, maxZ, minZ, distance, receivedazimuth, receiveddistance;
double azimuth;
int intensity = 255;
bool startCalibrating, startNavigating, stopNavigating;

byte received[7]; // number of bytes expected to receive

void setup(){

    Serial.begin(9600);
    bluetooth.begin(9600);
    Wire.begin();

    WriteWire(mpu95200address,0x37,0x02);
    WriteWire(magaddress,0x0A,0x16);

    maxX = EEPROMReadInt(0);
    minX = EEPROMReadInt(2);
    maxY = EEPROMReadInt(4);
    minY = EEPROMReadInt(6);
    maxZ = EEPROMReadInt(8);
    minZ = EEPROMReadInt(10);

    pinMode(M1, OUTPUT);
    pinMode(M2, OUTPUT);
    pinMode(M3, OUTPUT);
    pinMode(M4, OUTPUT);
    pinMode(Buzzer, OUTPUT);
   

    pinMode(2, INPUT_PULLUP);
    attachInterrupt(digitalPinToInterrupt(2), pressbutton, FALLING);
    analogWrite(Buzzer,128);
    delay(200);
    analogWrite(Buzzer,192);
    delay(200);
    analogWrite(Buzzer,128);
    delay(200);
    analogWrite(Buzzer,0);


}

void loop(){

 
    getRawData();
    //setLimits();
    nullOffset();
    a=getHeading(y,x);
    

    checkBluetooth();
     if(startNavigating){
        navigate();
    }
    
    delay(200);

  


}

void WriteWire(int addr, int innaddr, int data){

    Wire.beginTransmission(addr);
    Wire.write(innaddr);
    Wire.write(data);
    Wire.endTransmission();

}

void checkBluetooth(){
    i = 0; 
    if (bluetooth.available()){
        do{
    
      received[i] = bluetooth.read();
      i++;
      
    
    }while(i <= 6); // size of array -1
    

    receivedazimuth = BytesToInt(received[1], received[0]);
    receiveddistance = BytesToInt(received[3], received[2]);
    intensity = received[4];
    startNavigating = received[5];
    startCalibrating = received[6];
    

    if(startCalibrating){
        calibrate();
    }

    if(startNavigating){
        navigate();
    }

    startCalibrating = 0;
    startNavigating = 0;    
}
}

void calibrate(){

    do{
    getRawData();
    setLimits();
    checkBluetooth();
    delay(100);

    }while(startCalibrating);
    
    for (int i = 0 ; i < EEPROM.length() ; i++) {
    EEPROM.write(i, 0);
    } 
    
  EEPROMWriteInt(maxX, 0);
  EEPROMWriteInt(minX, 2);
  EEPROMWriteInt(maxY, 4);
  EEPROMWriteInt(minY, 6);
  EEPROMWriteInt(maxZ, 8);
  EEPROMWriteInt(minZ, 10);

}

void getRawData(){

    Wire.beginTransmission(magaddress);
    Wire.write(0x03);
    Wire.endTransmission();

    Wire.requestFrom(magaddress, 7);
    if(6 <= Wire.available()){
        x = Wire.read(); //LSB
        x |= Wire.read()*256; //MSB
        y = Wire.read();
        y |= Wire.read()*256;
        z = Wire.read();
        z |= Wire.read()*256;
        p = Wire.read();
    }
    
}

void nullOffset(){

    offsetX = (maxX - minX)/2;
    if((maxX + minX) > 1){
        x -= offsetX + minX;
    }
    else{
        x += offsetX - maxX;
    }

    offsetY = (maxY - minY)/2;
    if((maxY + minY) > 1){
        y -= offsetY + minY;
    }
    else{
        y += offsetY - maxY;
    }

    offsetZ = (maxZ - minZ)/2;
    if((maxZ + minZ) > 1){
        z -= offsetZ + minZ;
    }
    else{
        z += offsetZ - maxZ;
    }

}

void setLimits(){

    if(x > maxX){
        maxX = x;
    }

    if(x < minX){
        minX = x;
    }

    if(y > maxY){
        maxY = y;
    }

    if(y < minY){
        minY = y;
    }

    if(z > maxZ){
        maxZ = z;
    }

    if(z < minZ){
        minZ = z;
    }
}

double getHeading(int a, int b){
    double h;

    h = 90 - atan((float)a / (float)b) * 180 / PI;
    if (b < 0){
        h += 180;
    }

    return h;
}

void navigate(){

do{
    
   
    getRawData();
    nullOffset();
    a = getHeading(y,-x);

    a -= receivedazimuth;
    if(a < 0){
        a += 360;
    }

    if (a >= 338 || a <= 22) {
    setMotors(M2, M3);
  }
  
    if (a >= 23 && a <= 67) {
    setMotors(M3, 0);
  } 
    
    if (a >= 68 && a <= 112) {
    setMotors(M3, M4); 
  } 
    
    if (a >= 113 && a <= 157) {
    setMotors(M4, 0);
  } 

  if (a >= 158 && a <= 202) {
    setMotors(M1, M4);
  }
  
    if (a >= 203 && a <= 247) {
    setMotors(M1, 0);
  } 
    
    if (a >= 248 && a <= 292) {
    setMotors(M1, M2); 
  } 
    
    if (a >= 293 && a <= 337) {
    setMotors(M2, 0);
  } 

    delay(500);

}
while(startNavigating == 1);
    
}


void setMotors(int motor1, int motor2){
     
    


    analogWrite(motor1, intensity);
    if(motor2 > 0){
        analogWrite(motor2, intensity);
    }

    delay(100);

    analogWrite(motor1, 0);
    if(motor2 > 0){
        analogWrite(motor2, 0);
    }

    delay(100);

    
    
    analogWrite(motor1, intensity);
    if(motor2 > 0){
        analogWrite(motor2, intensity);
    }

    delay(100);
    analogWrite(motor1, 0);
    if(motor2 > 0){
        analogWrite(motor2, 0);
    }

}

void distancesignalization (){
    if(receiveddistance <= 200){
        delay(50);
    }

     if((receiveddistance > 200) && (receiveddistance < 1000) ){
        delay(150);
    }

     if(receiveddistance >= 1000){
        delay(250);
    }
    
}


void pressbutton(){

    delay(100);

    if(digitalRead(2)==LOW){
        if(startNavigating == LOW){
            startNavigating = 1;
            
        }
        else{
            startNavigating = LOW;
        }
        
    } 

}


int BytesToInt(byte MSB, byte LSB){
    
    int result;
    result = LSB;
    result |= MSB * 256;

    return result;
}

void EEPROMWriteInt(int toWrite, int addr){

    byte Lbyte = toWrite;
    EEPROM.write(addr, Lbyte);
    addr++;
    byte Hbyte = toWrite / 256;
    EEPROM.write(addr, Hbyte);
}

int EEPROMReadInt(int addr){
    int result = 0;

    result = EEPROM.read(addr);
    addr++;
    result |= EEPROM.read(addr)*256;

    return result;
}


byte BatteryCheck(){

    byte result = 0;

    result = analogRead(A7)/4;

    return result;

}