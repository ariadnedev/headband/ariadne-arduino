# Ariadne Headband Arduino Files

> Ariadne Headband is ongoing project that aims to create haptic navigation for blind people (among 
others). It uses 4 vibration motors placed in set directions on head and connected to Arduino board. 
Everything is controlled with Android app via Bluetooth. You can view our [Hackaday page](https://hackaday.io/project/160367-ariadne-headband)
to get more informations.

This repository contains arduino files for Ariadne Headband. Arduino board is controlled by Android app that is published in [different repository](https://gitlab.com/ariadnedev/headband/android-app/).


## License
```
Ariadne Headband Android Control App
Copyright (C) 2018  Tomáš Košíček

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
```